var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var ejs = require('ejs');

var index = require('./nav/index');
var books = require('./nav/books');

var listenPort = 9999;
var app = express();

// View Layer
app.set('views', path.join(__dirname, 'views'));
app.set('view_engine', 'ejs');
app.engine('html', ejs.renderFile);

// Set static folder
app.use(express.static(path.join(__dirname, 'client')));

// Body Pasrser MW
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use('/', index);
app.use('/services', books);

// Listen to Server
app.listen(listenPort, function() {
    console.log('Books server started on port:: ' + listenPort);
});