import { Component } from '@angular/core';
import {BookService} from './services/book.service';

@Component ({
  moduleId: module.id,
  selector: 'my-app',
  templateUrl: 'app.component.html',
  providers: [BookService]
})

export class AppComponent { }
