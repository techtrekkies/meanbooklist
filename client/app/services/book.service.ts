import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class BookService {
  constructor(private http:Http) {
    console.log('Book Service Initialized...');
  }

  getBooks() {
    return this.http.get('http://localhost:9999/services/books')
        .map(res => res.json())
  }

  addBook(newBook) {
    console.log("Adding new book... ");
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:9999/services/book', JSON.stringify(newBook), {headers: headers})
      .map(res => res.json());
  }

  deleteTask(id) {
    return this.http.delete('/services/book/'+id)
    .map(res => res.json());
  }
}
