import { Component } from '@angular/core';
import { BookService } from '../../services/book.service';
import { Book } from '../../../Book';

@Component ({
  moduleId: module.id,
  selector: 'books',
  templateUrl: 'books.component.html'
})

export class BooksComponent {
  books: Book[];
  name: string;
  author: string;
  constructor(private bookService:BookService) {
    this.bookService.getBooks()
      .subscribe(books => {
        console.log(books);
        this.books = books;
      });
  }

  addBook(event) {
    event.preventDefault();    
    console.log('Adding name - ' + this.name);    
    var newBook = {
      name: this.name,
      author: this.author
    }

    this.bookService.addBook(newBook)
      .subscribe(book => {
          this.books.push(book);
          this.name = '';
          this.author = '';
      });
  }

  deleteBook(id) {
    var books = this.books;
    this.bookService.deleteTask(id).subscribe(data => {
      if (data.n ==1) {
        for (var i=0;i<books.length; i++) {
          if (books[i]._id ==id) {
            books.splice(i, 1);
          }
        }
      }
    });
  }
}