var express = require('express');
var nav = express.Router();

nav.get('/', function(req, res, next) {
    res.render('index.html');
});

module.exports = nav;