var express = require('express');
var mongojs = require('mongojs');
var db = mongojs('mongodb://<user>:<password>@<clustername>.mlab.com:<port>/bookslist', ['books']);
var nav = express.Router();

// Get all books
nav.get('/books', function(req, res, next) {
    db.books.find(function(err, books) {
        if (err) {
            res.send(err);
        }
        res.json(books);
    });
});

// Find book
nav.get('/book/:id', function(req, res, next) {
    db.books.findOne({_id: mongojs.ObjectId(req.params.id)}, function(err, book) {
        if (err) {
            res.send(err);
        }
        res.json(book);
    });
});

// Save book
nav.post('/book', function(req, res, next) { 
    var book = req.body;
    if (!book.name && !book.author) {
        res.status(400);
        res.json({
            "error":"Bad data"
        });
    } else {
        db.books.save(book, function(err, book) {
            if (err) {
                res.send(err) ;            
            } else {
                res.json(book);
            }
        });
    }
});

// Delete book
nav.delete('/book/:id', function(req, res, next) {
    db.books.remove({_id: mongojs.ObjectId(req.params.id)}, function(err, book) {
        if (err) {
            res.send(err);
        } else {
            res.json(book)
        }
    });
})

//Update book
nav.put('/book/:id', function(req, res, next) {
    var book = req.body;
    var updBook = {};

    if (book.name) {
        updBook.name = book.name;
    } else {
        res.status(400);
        res.json({
            "error":"Bad data"
        });
    }

    if (book.author) {
        updBook.author = book.author;
    } else {
        res.status(400);
        res.json({
            "error":"Bad data"
        });
    }

    if (updBook) {
        db.books.update({_id: mongojs.ObjectId(req.params.id)}, updBook, {}, function(err, book) {
            if (err) {
                res.send(err)
            } else {
                res.json(book);
            }
        });
    } else {
        res.status(400);
        res.json({
            "error":"Bad data"
        });
    }
});

module.exports = nav;